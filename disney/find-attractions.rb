#! /usr/bin/env ruby
#
# Goal: when to go have little people
# Cheatsheet: http://www.rubyinside.com/nethttp-cheat-sheet-2940.html

# Make a list of attractions,
#  for each attractions, call and find out the "when_to_go" field

require 'json'
require 'uri'
require 'net/https'

# https://touringplans.com/magic-kingdom/attractions/haunted-mansion.json

def disneyHTTP(url)
  uri = URI.parse(url)
  http = Net::HTTP.new(uri.host, uri.port)
  http.use_ssl = true
  http.verify_mode = OpenSSL::SSL::VERIFY_NONE

  request = Net::HTTP::Get.new(uri.request_uri)
  response = http.request(request)
  return response.body
end

attractions = JSON.parse(disneyHTTP("https://touringplans.com/disneyland/attractions.json"))
attractions.each do |attraction|
  attractionDetails = JSON.parse(disneyHTTP("https://touringplans.com/disneyland/attractions/#{attraction['permalink']}.json"))
  puts "#{attraction} - #{attractionDetails['when_to_go']}"
end
