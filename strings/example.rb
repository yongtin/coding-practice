# larger_than("1232", "201") => True
# larger_than("202", "232") => False

def larger_than(strA, strB) 
  # take care of leading zeros
  lenA = strA.length 
  lenB = strB.length
  if lenA > lenB
    return true
  elsif lenB > lenA
    return false
  else
    idx=0
    while idx < lenA
      if strA[idx] > strB[idx]
        return true
      elsif strA[idx] < strB[idx]
        return false
      end
      idx+=1 
    end
    return false
  end
end
puts larger_than("1232", "201")
puts larger_than("202", "232")
puts larger_than("232748950376485902", "22134567432745632")
puts larger_than("0", "0")
