#!/bin/ruby


n = gets.strip.to_i
a = gets.strip
a = a.split(' ').map(&:to_i)

i=0
swaps = 0;
while i < n
    j=0
    while j < n-1
        if a[j] > a[j+1] 
            temp = a[j]
            a[j]=a[j+1]
            a[j+1]=temp
            swaps+=1
        end
        j+=1
    end
    i+=1
    break if swaps == 0
end
puts "Array is sorted in #{swaps} swaps."
puts "First Element: #{a.first}"
puts "Last Element: #{a.last}"
