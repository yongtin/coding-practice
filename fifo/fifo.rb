# Enter your code here. Read input from STDIN. Print output to STDOUT

queue = Array.new
$stdin.readline # ignore first line
$stdin.readlines.each do |command|
    type_str, value = command.split(" ")
    type = type_str.to_i
    if type == 1 # enqueue
      queue << value
    elsif type == 2 # dequeue
      queue.shift # remove first element
    elsif type == 3 # print 
      puts queue[0]
    end
end

