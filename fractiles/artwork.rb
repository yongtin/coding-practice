#!/usr/bin/env ruby
# K = 2 (sequence)
# C = 3 (complexity/level)
# S = 2 (students)

def expandOriginal(k)
  m = Array.new
  for i in 0..(2**k)-1
    # Matrix = [ [0, 0, 0], [0, 0, 1], [0, 1, 0], ... ]
    # [ 0,0,0 ] = i.to_s(2).rjust(k,"0").scan(/\d/)
    m[i] = i.to_s(2).rjust(k,"0")
  end
  return m
end

# recursive: original = 01, o = 01, c = 1, out = 0111
def transformTile(o, curr, c, k)
  out = ""
  for tile in curr.scan(/\d/)
    t = tile.to_i
    if t == 0 # lead
      out = out + o
    elsif t == 1 # gold
      out = out + (2**k-1).to_s(2)
    end
  end
  if c > 1
    return transformTile(o, out, c-1, k)
  else
    return out
  end
end

fp = File.open("D-small-practice.in", "r")
fp.readline
testCase = 0
fp.readlines.each do |line|
  testCase += 1
  k_s, c_s, s_s = line.split(" ")
  k = k_s.to_i
  c = c_s.to_i
  s = s_s.to_i
  original = expandOriginal(k)
  idx = 0
  expandedOriginal = Array.new
  original.each do |o|
    expandedOriginal[idx] = transformTile(o, o, c-1, k).scan(/\d/)
    idx+=1
  end
  sizeofJ = expandedOriginal[0].length

  goodColumn = []
  for j in 0..sizeofJ-1
    count = 0
    for i in 0..2**k-1
      if expandedOriginal[i][j] == 0
        count += 1
      end
      if count >= s
        break
      end
    end
    goodColumn << j+1
    if goodColumn.length == s
      puts "Case ##{testCase}: #{goodColumn.join(' ')}"
      break
    end
  end
  if goodColumn.length < s
    puts "Case ##{testCase}: IMPOSSIBLE"
  end
end
