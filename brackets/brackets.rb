#!/bin/ruby

def isBalance(expression)
        pair = []
        i = 0
        while i < expression.length
            if expression[i] == "{"
                pair << "}"
            elsif expression[i] == "("
                pair << ")"
            elsif expression[i] == "["
                pair << "]"
            else
                if expression[i] != pair.last or pair.empty? 
                    return false
                end
                pair.pop
            end
            i+=1
        end
        return pair.empty?
end

t = gets.strip.to_i
for a0 in (0..t-1)
    expression = gets.strip
    i = 0
    if expression.length % 2 == 1
      puts "NO"
    else
        if isBalance(expression)
            puts "YES" 
        else
            puts "NO"
        end
    end
end


