#!/usr/bin/env ruby
def lineToPancakes(pancakes)
  return pancakes.scan(/\W/)
end
fp = File.open("B-large-practice.in", "r")
fp.readline

testCase = 0
fp.readlines.each do |l|
  testCase += 1
  pancakes = lineToPancakes(l) # pancakes = [ "-","+",... ]
  if pancakes.length == 1
    if pancakes[0] == "-"
      puts "Case ##{testCase}: 1" 
    else
      puts "Case ##{testCase}: 0" 
    end
    break
  end

  flips = 0
  sideup = false # true = happy, false = blank
  if pancakes[0] == "+"
    sideup = true
  else
    sideup = false
    flips = 1
  end
  # -+
  # +-
  pancakes.shift
  pancakes.each do |pancake|
    flips += 2 if sideup == true and pancake == "-"
    if pancake == "+"
      sideup = true
    else
      sideup = false
    end
  end
  puts "Case ##{testCase}: #{flips}" 
end

