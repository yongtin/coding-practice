#!/usr/bin/env ruby

def gcd(a, b)
  gcdnum = -1
  if a == 0
    gcdnum = b
  elsif b == 0
    gcdnum = a
  elsif a > b
    gcdnum = b
    for i in 1..b
      r1 = a.to_f/i
      r2 = b.to_f/i
      if (r1 * 10) % 10 == 0 and (r2 * 10) % 10 == 0
        gcdnum = i
      end
      i -= 1
    end
  else 
    gcdnum = a
    for i in 1..a
      r1 = a.to_f/i
      r2 = b.to_f/i
      if (r1 * 10) % 10 == 0 and (r2 * 10) % 10 == 0
        gcdnum = i
      end
      i -= 1
    end
  end
  return gcdnum
end

puts gcd(0,0)
