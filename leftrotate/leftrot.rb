#!/usr/bin/env ruby

newAry = Array.new
fp = File.open("input.txt", "r")
l = fp.readline
origAry = fp.readline.split(" ")
if l =~ /^(\d) (\d)/
  n = $1.to_i
  d = $2.to_i
  pos = 0 
  origAry.each do |i|
    if pos < d
      newAry[n - d + pos] = origAry[pos]
    else
      newAry[pos - d] = origAry[pos]
    end
    pos += 1
  end
end
puts newAry.join(" ")
