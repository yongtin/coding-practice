#!/usr/bin/env ruby
require 'prime'
# 1001 - 9, 28, 65, 126, 217, 344, 513, 730, and 1001

def getPrimeFromCoin(coin)
  proof = coin + " "
  for base in 2..10
    num = coin.to_i(base)
    proofArray = Prime.prime_division(num)
    if proofArray.length == 1 # is prime number
      return true, ""
    end
    proof = proof + proofArray[0][0].to_s + " "# one of the factors
  end
  return false, proof
end

J = 50
N = 16
puts "Case #1: " 
# generating each coin
idx = 0 # will overflow
for i in 0..J-1

  # based on loop status, generate a string
  while true # overflow - need to check N
    # 1 + leading zero's idx, with length == N + 1
    # N = 6, idx = 5, coin = 101011
    coin = "1" + idx.to_s(2).rjust(N-2, "0") + "1"
    isPrime, proof = getPrimeFromCoin(coin) 
    idx += 1
    if isPrime
      next
    else
      puts proof
      break
    end
  end
end

