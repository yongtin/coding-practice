#!/usr/bin/ruby

m,n = gets.strip.split(' ')
m = m.to_i
n = n.to_i
magazine = gets.strip
magazine = magazine.split(' ')
ransom = gets.strip
ransom = ransom.split(' ')

# building the dictionary
words = Hash.new(0)
magazine.each do |w|
  words[w] += 1
end

ransom.each do |w|
  if words.has_key?(w) 
    words[w] -= 1
    if words[w] < 0
      print "No"
      exit 0
    end
  else
    print "No"
    exit 0
  end
end
print "Yes"
