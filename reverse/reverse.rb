#!/usr/bin/env ruby
#

# in ["dog", "god", "tea"]
# out [ "dog", "god" ]

def examine(word)
  return word.scan(/\w/).reverse.join('')
end

input = ["dog", "god", "tea"]
input = ["2dc", "god", "tea"]
input = ["tsang", "gnast", "tea", "aet", "god", "dog"]

seen = []
out = []
input.each do |i|
  seen << i
  c = examine(i)
  if seen.include?(c)
    out << i
    out << c
  end
end

puts out
