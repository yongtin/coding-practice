#!/bin/ruby

a = gets.strip
b = gets.strip

a_sort = a.split("").sort
b_sort = b.split("").sort

i=0
j=0
deletes = 0
while i < a_sort.length or j < b_sort.length do
    if i == a_sort.length
        j+=1
        deletes +=1
    elsif j == b_sort.length
        i+=1
        deletes +=1
    elsif a_sort[i] > b_sort[j]
        j+=1
        deletes += 1
    elsif b_sort[j] > a_sort[i]
        i+=1
        deletes += 1
    else
        i+=1
        j+=1
    end
end
puts deletes
