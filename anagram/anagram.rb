#!/bin/ruby

a = gets.strip
b = gets.strip

a_sort = a.split('').sort
b_sort = b.split('').sort

i=0
j=0

deletion_count = 0
while (i < a.length or j < b.length) do
    if i >= a.length and j <= b.length
        deletion_count += 1
        j+=1
    elsif j >= b.length and i <= a.length
        deletion_count += 1
        i+=1
    elsif a_sort[i] < b_sort[j] 
        deletion_count += 1
        i+=1
    elsif a_sort[i] > b_sort[j]
        deletion_count += 1
        j+=1
    elsif a_sort[i] == b_sort[j]
        j+=1
        i+=1
    end
end
puts deletion_count
