#!/usr/bin/env ruby
#

def plusOne(a)
    total = 0
    for i in 0..a.length-1
        total += (10**(a.length-1-i))*(a[i])
        i += 1
    end
    total += 1
    return total.to_s.chars
end
puts plusOne([1,2,3])
