# @param {Integer[]} nums
# @param {Integer} k
# @return {Integer[]}
def top_k_frequent(nums, k)
    dict = Hash.new(0)
    out = Array.new
    nums.each do |num|
        dict[num] += 1
    end
    d = dict.sort_by{|num,freq| freq}.reverse
    
    i = 0
    d.each do |num,freq|
        out << num
        i += 1
        if i == k
            return out
        end
    end
    return out
end
