#!/usr/bin/env ruby

# read first line and define the array
# allocate the array based on size in first line

#read each lines
#  caseNumber = lineNumber
#  for 0 <= N <= 200
#    collection = []
#    lineOutput = input * N
#    collection << getDigitsFromOutput(lineOutput)
#    puts "Case ##{caseNumber}: #{lineOutput*N}" if isCompleted?(collection)
#  
  
def isCompleted?(ary)
  ary.length == 10
end

def getDigitsFromOutput(ary,num)
  return getDigit(ary, num).sort.uniq
end

def getDigit(ary, num)
  iter = num
  while ( iter != 0 )
    ary << iter % 10
    iter = iter/10
  end
  return ary
end

fp = File.open("./A-large-practice.in", "r")
fp.readline
testNumber = 0
fp.readlines.each do |l|
  collection = []
  testNumber += 1
  caseNumber = l.to_i
  for n in 0..10**6
    lineOutput=caseNumber*n
    for i in getDigitsFromOutput(collection, lineOutput)
      collection << i
    end
    if isCompleted?(collection.sort.uniq)
      puts "Case ##{testNumber}: #{lineOutput}" 
      break
    end
  end
  if not isCompleted?(collection.sort.uniq)
    puts "Case ##{testNumber}: INSOMNIA"
  end
end
fp.close()
