""" Node is defined as
class node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None
"""

def checkBST(root):
  if root.left == None and root.right == None:
    return True

  lowerlimit = 0
  upperlimit = 10**4
  if root.left.data < root.data and root.data < root.right.data:
    return checkTreeBST(root.left, lowerlimit, root.data) and checkTreeBST(root.right, root.data, upperlimit)
  else:
    return False

def checkTreeBST(leaf, lowerlimit, upperlimit):
  if leaf.left == None and leaf.right == None:
    if lowerlimit < leaf.data and leaf.data < upperlimit:
      return True
    else:
      return False

  else:
    if leaf.left.data < leaf.data and leaf.data < leaf.right.data and lowerlimit < leaf.data and leaf.data < upperlimit:
      return checkTreeBST(leaf.left, lowerlimit, leaf.data) and checkTreeBST(leaf.right, leaf.data, upperlimit)
    else:
      return False



